# Vue.js Container Usage Guide with Docker

This guide provides detailed instructions for building, launching, and using the Docker container for your Vue.js application. Make sure you have Docker installed on your machine before getting started.

## Guide Contents
1. [Docker Configuration](#docker-configuration)
2. [Container Build](#container-build)
3. [Container Launch](#container-launch)
4. [Using Docker Options](#using-docker-options)

---

## Docker Configuration

The `docker-compose.yml` file is a configuration file that defines, configures, and runs multiple containers simultaneously. It specifies services, networks, volumes, and other configurations necessary for container orchestration. This file typically uses a set of YAML rules to describe relationships between different services.

```yaml
version: "3.9"
services:

  front:
    container_name: container_front
    build: .
    ports:
      - "8080:8080"
    networks:
      - my-networks
    volumes:
      - .:/app

  nginx:
    container_name: container_nginx
    image: nginx:latest
    ports:
      - "443:443"
    volumes:
      - ./certs:/certs
      - ./nginx.conf:/etc/nginx/conf.d/default.conf
    networks:
      - my-networks

networks:
  my-networks:
```

The `Dockerfile` is a script containing instructions for setting up an environment. These instructions specify how to build an image from a base image, configure the environment, copy necessary files, install dependencies...

```Dockerfile
FROM node:latest

WORKDIR /app

COPY . /app

RUN npm install

EXPOSE 8080

CMD ["npm", "run", "serve"]
```

---

## Container Build and Launch

To build the image and launch the container(s) in the background, follow these steps:

1. Ensure you are in the directory containing the `docker-compose.yml` file.
2. Run the following command in your terminal:

   ```bash
   docker-compose up -d --build
   ```

---

## Using Docker Options

### Using a Volume

The Docker configuration uses a volume to link the local directory to `/app` in the container. This allows real-time updates to the Vue.js application without having to rebuild the container for every modification.